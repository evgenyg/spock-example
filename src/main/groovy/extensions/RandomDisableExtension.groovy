package extensions

import org.spockframework.runtime.extension.IGlobalExtension
import org.spockframework.runtime.model.SpecInfo


/**
 * Global extensions randomly disabling specs and feature methods.
 */
class RandomDisableExtension implements IGlobalExtension
{
    private final Random random = new Random( System.currentTimeMillis())


    void visitSpec ( SpecInfo spec )
    {
        final randomBoolean              = { int j -> random.nextInt( 100 ) < j }
        spec.excluded                    = randomBoolean( 5  )
        spec.features.each { it.excluded = randomBoolean( 15 ) }
        spec.features.each { it.skipped  = randomBoolean( 20 ) }
    }
}
