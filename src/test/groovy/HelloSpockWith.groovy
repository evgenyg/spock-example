import com.github.goldin.spock.extensions.with.With
import spock.lang.Unroll

/*
* Copyright 2009 the original author or authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

@With({ [ [ 1 : 2, 3 : 4 ], 'something' ] })
class HelloSpockWith extends spock.lang.Specification {

    def setup      () { println 'setup()'       }
    def cleanup    () { println 'cleanup()'     }
    def setupSpec  () { println 'setupSpec()'   }
    def cleanupSpec() { println 'cleanupSpec()' }

    @Unroll
    @With({ [ 5 ] })
    def "length of Spock's and his friends' names: [#name] / [#length]"() {
        expect:
        name.size()  == length
        size()       == 2                 // Map
        getAt( 1 )   == 2                 // Map
        floatValue() == 5.0F              // Integer
        bytes        == 'something'.bytes // String

        where:
        name   << [ 'Spock', 'Kirk', 'Scotty' ]
        length << [ 5, 4, 6 ]
    }
}

