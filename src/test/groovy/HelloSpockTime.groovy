import spock.lang.Unroll
import com.github.goldin.spock.extensions.time.Time

/*
* Copyright 2009 the original author or authors.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

@Time( min = 50, max = 1000 )
class HelloSpockTime extends spock.lang.Specification {

    def setup      () { println 'setup()'       }
    def cleanup    () { println 'cleanup()'     }
    def setupSpec  () { println 'setupSpec()'   }
    def cleanupSpec() { println 'cleanupSpec()' }

    @Unroll
    def "length of Spock's and his friends' names: [#name] / [#length]"() {
        expect:
        name.size() == length
        sleep( 50 )

        where:
        name   << [ 'Spock', 'Kirk', 'Scotty' ]
        length << [ 5, 4, 6 ]
    }
}

